oldPullEvent = os.pullEvent
os.pullEvent = os.pullEventRaw

local RegistryStructure = {"os/registry", "os/registry/libs"}

local IsRegistryBuilt = true

local OS_NAME = "EdithOS v1.4"

local username = "admin"
local password = "password"

for i=1,#RegistryStructure do
    if not fs.exists(RegistryStructure[i]) then
        IsRegistryBuilt = false
    end
end

function printHeader(id)
    print("-***--------")
    print("*-E-*-------")
    print("-***--------")
    print("------------")
    print("------------")
    print(" ")

    if id == 0 then
        write("Authentication Key: ")
    elseif id == 1 then
        local time = os.time()
        local formattedTime = textutils.formatTime(time, false)

        local updateHeader = "Up To Date!"
        local local_version = generic.strSplit(OS_NAME, " ")
        local server_version = networking.getServerOSVersion()
        if server_version ~= local_version[2] then
            updateHeader = "Update Available!"
        end

        print(OS_NAME .. " | " .. generic.getUsername() .. " | " .. formattedTime .. " | " .. updateHeader)
        print(" ")
        if generic.getModem() ~= nil then
            rednet.open(generic.getModem())
            shell.openTab("net")
        end
    end
end

if IsRegistryBuilt == false then
    print("CORRUPTED INSTALL")
    sleep(2)
    os.reboot()
else
    os.loadAPI("os/registry/libs/generic")
    os.loadAPI("os/registry/libs/base64")
    os.loadAPI("os/registry/libs/networking")

    local regFiles = fs.list("os/registry")
    for fi=1,#regFiles do
        if generic.strStartsWith(regFiles[fi], "p_") then
            local passString = generic.strSplit(regFiles[fi], "_")
            password = base64.decode(passString[2])
        elseif generic.strStartsWith(regFiles[fi], "u_") then
            local userString = generic.strSplit(regFiles[fi], "_")
            username = userString[2]
        end
    end

    term.clear()
    term.setCursorPos(1, 1)

    printHeader(0)
    input = read("*")
    if input == password then
        print("Authenticated!")
        print(" ")
        sleep(2)
        term.clear()
        term.setCursorPos(1, 1)
        printHeader(1)
    else
        print("Incorrect key specified...")
        sleep(2)
        os.reboot()
    end
end

os.pullEvent = oldPullEvent