os.loadAPI("disk/libs/base64.lua")

local args = { ... }

term.clear()

if #args ~= 2 then
    print("Invalid arguments for OS registry(UNAME, PASS)")
    return
else
    print("Installing OS...")

    local RegistryStructure = {"os/registry", "os/registry/libs", "os/registry/p_", "os/registry/u_"}
    
    for i=1,#RegistryStructure do
        if not fs.exists(RegistryStructure[i]) then
            fs.makeDir(RegistryStructure[i])
        end
    end

    fs.copy("disk/libs/generic.lua", "os/registry/libs/generic")
    fs.copy("disk/libs/base64.lua", "os/registry/libs/base64")
    fs.copy("disk/libs/json.lua", "os/registry/libs/json")
    fs.copy("disk/libs/networking.lua", "os/registry/libs/networking")

    fs.move("os/registry/p_", "os/registry/p_" .. base64.encode(args[2]))
    fs.move("os/registry/u_", "os/registry/u_" .. args[1])

    sleep(1)

    fs.copy("disk/libs/tar.lua", "tar")
    fs.copy("disk/update.lua", "update")
    fs.copy("disk/edith-bl.lua", "startup")

    if fs.exists("startup") then
        print("OS installed, rebooting...")
        sleep(1)
        os.reboot()
    else
        print("Failed to install OS, check root dir!")
    end
    os.unloadAPI("disk/libs/base64.lua")
end