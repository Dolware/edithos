os.loadAPI("os/registry/libs/generic")
os.loadAPI("os/registry/libs/base64")

local username = generic.getUsername()
local password = ""
local regFiles = fs.list("os/registry")
for fi=1,#regFiles do
    if generic.strStartsWith(regFiles[fi], "p_") then
        local passString = generic.strSplit(regFiles[fi], "_")
        password = base64.decode(passString[2])
    end
end

shell.run("rm", "os")
shell.run("rm", "startup")
shell.run("rm", "disk/*")
shell.run("wget", "http://dolware.cf/CC/EDITH.tar", "disk/EDITH.tar")
shell.run("cd", "disk")
shell.run("../tar", "-xvf", "EDITH.tar")
shell.run("cd", "../")
shell.run("rm", "disk/EDITH.tar")
shell.run("rm", "tar")
shell.run("rm", "update")
shell.run("disk/install", username, password)