os.loadAPI("os/registry/libs/generic")

term.clear()
term.setCursorPos(1, 1)


print("[ACHILLES] > Listening(CONSUMER)...")

SAVED_SENDER = 0


CMD_PING = {
    cmd_trigger="ping",
    cmd_desc="ping",
    cmd_useArgs=false,
    cmd_exec = function(s)
        rednet.send(s, "pong")
    end
}

CMD_TRACE = {
    cmd_trigger="trace",
    cmd_desc="trace|id",
    cmd_useArgs=true,
    cmd_exec = function(s, a)
        SAVED_SENDER = s
        local a_s = generic.strSplit(a, "|")
        if #a_s > 0 then
            if tonumber(a_s[1]) == tonumber(os.getComputerID()) then
                rednet.send(s, "trace_pong|" .. os.getComputerID())
            else
                rednet.broadcast("trace|" .. a_s[1])
            end
        end
    end
}

CMD_TRACE_PONG = {
    cmd_trigger="trace_pong",
    cmd_desc="trace_pong|id",
    
    cmd_useArgs=false,
    cmd_exec = function(s, a)
        rednet.send(SAVED_SENDER, "trace-pong|" .. os.getComputerID())
    end
}

ALL_CMDS = {}

table.insert(ALL_CMDS, CMD_PING)
table.insert(ALL_CMDS, CMD_TRACE)
table.insert(ALL_CMDS, CMD_TRACE_PONG)

while true do
    sID,msg,proto = rednet.receive()

    if string.find(msg, "|") then
        local m_s = generic.strSplit(msg, "|")
        for i=1,#ALL_CMDS do
            if ALL_CMDS[i].cmd_trigger == m_s[1] then
                ALL_CMDS[i].cmd_exec(sID, msg);
            end
        end
    else
        for i=1,#ALL_CMDS do
            if ALL_CMDS[i].cmd_trigger == msg then
                ALL_CMDS[i].cmd_exec(sID);
            end
        end
    end
end

if generic.getModem() ~= nil then
    rednet.close(generic.getModem())
end