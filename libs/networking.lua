function getServerOSVersion()
    local wh = http.get("https://bitbucket.org/Dolware/edithos/raw/4a0838001a089f4083a1fd93ca64e9fcc035a645/server_version.txt")
    local ver = "UNKNOWN_VERSION"
    if wh then
        ver = wh.readAll()
    else
        ver = "UNKNOWN_VERSION"
    end

    return ver
end